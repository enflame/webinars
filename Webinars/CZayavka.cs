﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using MySql;
namespace Webinars
{
  class CZayavka
  {
    CMySql _mySql;

    string ID_PER;
    public CZayavka(CMySql mySql)
    {
      _mySql = mySql;

    }

    public List<Period_str> GetPeriod_spr()
    {
      string sql = " select * from cdo.period_spr";
      DataTable dt = _mySql.SqlSelect(sql);
      List<Period_str> res = new List<Period_str>();
      foreach (DataRow row in dt.Rows) res.Add(new Period_str(row));
      return res;
    }
    public List<CTutor> GetTutors(string byear, string eyear, int numSem)
    {
      //string sql = " select distinct TL.TUTOR_ID, t.lastname||' '||firsname||' '||midlename as fio" +
      //      " from TUTORING_LOAD tl" +
      //      " left join tutor T on T.ID = TL.TUTOR_ID" +
      //      " where TL.DATE_REG >= to_date('" + byear + "', 'yyyy') and " + " TL.DATE_REG <= to_date('" + eyear + "', 'yyyy')" +
      //      " order by   TL.DATE_REG desc, fio";

      string sql = "select * from period_spr ps" +
              " where ps.byear = " +byear + " and ps.eyear = " + eyear;
      DataTable dt = _mySql.SqlSelect(sql);
      if (dt.Rows.Count == 0)
      {
        return null;
      }
      ID_PER = dt.Rows[0]["id_per"].ToString();

      sql = "select distinct T.ID , t.lastname||' '||firsname||' '||midlename as fio" +
              " from tutor T" +
              " where T.ID in (select distinct TL.TUTOR_ID from TUTORING_LOAD tl where TL.ID_PER = " + ID_PER + ")" +
              " order by fio";

      dt = _mySql.SqlSelect(sql);
      List<CTutor> res = new List<CTutor>();
      foreach (DataRow row in dt.Rows)
      {
        CTutor tutor = new CTutor(row);
        tutor.Disc = GetDisc(tutor.TUTOR_ID, numSem);
        if (tutor.Disc.Count>0) 
          tutor.GROUPS = GetGroups(tutor.TUTOR_ID, tutor.Disc[0].ID, numSem);
        res.Add(tutor);
      }
      return res;
    }
    List<CDisc> GetDisc(string TUTOR_ID, int numSem)
    {
      string sql = " select distinct sd.dis_full_name as disc_name, sd.id" +
            " from S_DISCIPLINE sd" +
            " where SD.ID in (select distinct TL.DISCIPLINE_ID from TUTORING_LOAD tl  where "+
            " TL.ID_PER = " + ID_PER + 
            " and TL.TUTOR_ID = " + TUTOR_ID + "and TL.HOURS>=0 and TL.SEMESTR=" + numSem.ToString() + ")" +
            " order by disc_name";
      DataTable dt = _mySql.SqlSelect(sql);
      List<CDisc> res = new List<CDisc>();
      foreach (DataRow row in dt.Rows)
      {
        res.Add(new CDisc(row["id"].ToString(), row["disc_name"].ToString()));
      }
      return res;
    }
    public List<string> GetGroups(string TUTOR_ID, string disc_id, int numSem)
    {
      string sql = "select  distinct ID as GROUP_ID from CDO.STUD_GROUP sg where SG.ID in " +
                   " (select distinct GROUP_ID from TUTORING_LOAD_GROUPS tlg " +
                   " where TLG.ID_TUT_LOAD in " +
                   "(select distinct TL.ID_TUT_LOAD from TUTORING_LOAD tl  where TL.TUTOR_ID = " + TUTOR_ID +
                   " and discipline_id=" + disc_id +
                   " and TL.ID_PER = " + ID_PER + " and TL.SEMESTR=" + numSem + " ))";// +" and SG.IS_DIST = 'T'";
      if (disc_id == "") sql = "select  distinct ID as GROUP_ID from CDO.STUD_GROUP sg where SG.ID in " +
                   " (select distinct GROUP_ID from TUTORING_LOAD_GROUPS tlg " +  
                   " where TLG.ID_TUT_LOAD in " +
                   "(select distinct TL.ID_TUT_LOAD from TUTORING_LOAD tl  where TL.TUTOR_ID = " + TUTOR_ID +
                   "and (TL.ID_PER = " + ID_PER + " and TL.SEMESTR=" + numSem + " )))";// +" and SG.IS_DIST = 'T'";
      DataTable dt = _mySql.SqlSelect(sql);

      List<string> res = new List<string>();
      foreach (DataRow row in dt.Rows)
      {
        res.Add(row["GROUP_ID"].ToString());
      }
      return res;
    }

    public void Make(CTutor tutor)
    {

    }
    public string to_date(string yyyy, string MM, string dd)
    {
      return "to_date('" + yyyy + "." + MM + "." + dd + "', 'yyyy.MM.dd')";
    }
    public FreeDays FindFree(string groups, string yyyy, string MM, string dd)
    {
      //  string groups = tutor.GroupsToStr();
      string dt = to_date(yyyy, MM, dd);
      string sql = "select * from web_total wtl" +
                   " join Web_groups wg on wg.id_web_total = wtl.id" + 
                   " join web_times wts on wts.id=wtl.id_web_times" +
                   " join web_auds was on was.id=wtl.id_web_auds" +
                   " where wg.group_id in (" + groups + ") and wtl.dt=" + dt;
      DataTable table = _mySql.SqlSelect(sql);

      FreeDays daysBisy = new FreeDays();
      foreach (DataRow row in table.Rows)
      {
        DateTime dtr = (DateTime)row["dt"];
        daysBisy.AddDay(dtr.Day, row["id_web_auds"].ToString(), row["aud"].ToString(), row["id_web_times"].ToString(), row["time"].ToString(), -1,-1, 1);
      }


      sql = "select * from web_times";
      DataTable t_times = _mySql.SqlSelect(sql);
      sql = "select * from web_auds";
      DataTable t_auds = _mySql.SqlSelect(sql);

      FreeDays daysFree = new FreeDays();
    //  for (int day = DateTime.Now.Day; day <= DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); day++)
    //  int day = DateTime.Now.Day;
      int day = Convert.ToInt32(dd);
      {
        foreach (DataRow r_aud in t_auds.Rows)
        { 
          string id_web_auds = r_aud["id"].ToString();
          string aud = r_aud["aud"].ToString();
          foreach (DataRow r_time in t_times.Rows)
          {
            string id_web_times = r_time["id"].ToString();
            string time = r_time["time"].ToString();

            sql = "select count(*) from web_total where dt =  " + to_date(yyyy, MM, day.ToString()) + " and id_web_auds=" + r_aud["id"].ToString() + " and id_web_times=" + r_time["id"].ToString() + " and ID_TUTOR is not null";
            DataTable tb = _mySql.SqlSelect(sql);

            if (!daysBisy.IsGroupsInRes(day, aud, time))
              daysFree.AddDay(day, id_web_auds, aud, id_web_times, time, Convert.ToInt32(tb.Rows[0][0].ToString()), Convert.ToInt32(r_aud["count"].ToString()), 0);
            else
              daysFree.AddDay(day, id_web_auds, aud, id_web_times, time, Convert.ToInt32(tb.Rows[0][0].ToString()), Convert.ToInt32(r_aud["count"].ToString()), 1);
          }
        }
      }
      return daysFree;
    }

    //public DataTable FindFreeT(CTutor tutor, string groups)
    //{
    //  DataTable res = new DataTable();
    //  res.Columns.Add("day");
    //  res.Columns.Add("aud");
    //  res.Columns.Add("time");
    //  FreeDays freeDays = FindFree(tutor, groups);
    //  foreach (FreeDay day in freeDays.freeRes)
    //    foreach (FreeAud aud in day.Auds)
    //      foreach (string t in aud.times)
    //      {
    //        DataRow r = res.NewRow();
    //        r["day"] = day.day;
    //        r["aud"] = aud.aud;
    //        r["time"] = t;

    //        res.Rows.Add(r);
    //      }

    //  return res;
    //}
  }

  public class Period_str
  {
    public string Period { get; set; }
    public string BYear { get; set; }
    public string EYear { get; set; }

    public Period_str(DataRow row)
    {
      Period = row["Period_str"].ToString();
      BYear = row["BYear"].ToString();
      EYear = row["EYear"].ToString();
    }
  }
  public class CTutor
  {
    public string TUTOR_ID { get; set; }
    public string FIO { get; set; }
    public string DATE_REG { get; set; }
    public List<string> GROUPS { get; set; } //группы для выбранной дисц. (иногда нужен список для всех дисц.)
    public List<CDisc> Disc { get; set; }

    public CTutor(DataRow row)
    {
      TUTOR_ID = row["ID"].ToString();
      FIO = row["FIO"].ToString();
    }
    public string GroupsToStr()
    {
      string res = "";
      foreach (string s in GROUPS)
        res += "'" + s + "',";
      int ind = res.LastIndexOf(',');

      return res.Substring(0, ind);
    }
  
  }

  public class CDisc
  {
    public string ID { get; set; }
    public string dis_full_name { get; set; }

    public CDisc(string id, string name)
    {
      ID = id;
      dis_full_name = name;
    }
  }
  public class CGroup
  {
    public string id { get; set; }
    //   public string dis_full_name { get; set; }
  }

  public class FreeDays
  {
    public List<FreeDay> freeRes;

    public FreeDays()
    {
      freeRes = new List<FreeDay>();
    }

    public void AddDay(int day, string id_web_auds, string aud, string id_web_times, string time, int countTutors, int countPlaces, int IsBusy)
    {
      FreeDay r = new FreeDay();
 //     r.Day = day;
      r.id_web_auds = id_web_auds;
      r.Aud = aud;
      r.id_web_times = id_web_times;
      r.Time = time;
      r.IsBusy = IsBusy;

      r.CountTutors = countTutors;
      r.CountPlaces = countPlaces;
      freeRes.Add(r);
    }

    public bool IsGroupsInRes(int day, string aud, string time) //для набора групп
    {
      foreach (FreeDay res in freeRes)
        if (res.Time == time)
          //if ((res.Day == day) & (res.Time == time))
          //if ((res.Day == day) & (res.Aud == aud) & (res.Time == time))
          return true;
      return false;
    }
  }

  public class FreeAud
  {
    public string aud;
    public List<string> times;

    public FreeAud()
    {
      times = new List<string>();
    }
    public void AddTime(string time)
    {
      foreach (string t in times)
        if (t == time) return;

      times.Add(time);
    }

  }
  public class FreeTimes
  {
    public string time;
    public int Count;//занято

    public FreeTimes()
    { }
  }
  public class FreeDay //для набора групп!!!!!
  {
 //   public int Day { get; set; }
    public string id_web_auds;
    public string Aud { get; set; }
    public string id_web_times;
    public string Time { get; set; }
    public int IsBusy { get; set; }
    public int id_tutor;

    public int CountPlaces { get; set; }
    public int CountTutors { get; set; }

    public int CountFreePlaces { get { return CountPlaces - CountTutors; } } //количество заявок на выбранную дату, аудиторию и время
    public string CountTutorsBusy { get { return CountTutors  + "\\" + CountPlaces; } }


    public FreeDay()
    {
    }

  }
}
