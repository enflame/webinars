﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using MySql;
namespace Webinars
{
  /// <summary>
  /// Логика взаимодействия для wndImportNotFound.xaml
  /// </summary>
  public partial class wndImportNotFound : Window
  {
    string _type;
    public string ID;
    public bool isToAll;
    public CReplace replace;
    public wndImportNotFound(string type, string element, CMySql mysql, int numRow, string addit = "")
    {
      InitializeComponent();

      tb_element.Text = element;
      tb_numrow.Text = "№ строки: " + numRow.ToString();

      if (type == "tutor")
      {
        string sql = "select distinct T.ID , t.lastname||' '||firsname||' '||midlename as fio" +
                " from tutor T" +
                " order by fio";
        DataTable dt = mysql.SqlSelect(sql);
        dg.ItemsSource = dt.DefaultView;
        //    cb_change.DisplayMemberPath = "Period";


      }
      if (type == "group")
      {
   //     string sql = "select distinct id from stud_group where id='" + element + "'";
        string sql = "select distinct id from stud_group order by id";
        DataTable dt = mysql.SqlSelect(sql);
        dg.ItemsSource = dt.DefaultView;
      }

      if (type == "disc")
      {
        string sql = " select distinct sd.dis_full_name as disc_name, sd.id" +
            " from S_DISCIPLINE sd" +
            " where SD.ID in (select distinct TL.DISCIPLINE_ID from TUTORING_LOAD tl  where TL.TUTOR_ID = " + addit + ")" +
            " order by disc_name";
        DataTable dt = mysql.SqlSelect(sql);
        if (dt.Rows.Count == 0) 
        {
           sql = " select distinct sd.dis_full_name as disc_name, sd.id" +
             " from S_DISCIPLINE sd" +
             " order by disc_name";
           dt = mysql.SqlSelect(sql);
        }
        dg.ItemsSource = dt.DefaultView;
      }

      if (type == "time")
      {
        string sql = "select * from web_times";
        DataTable dt = mysql.SqlSelect(sql);
        dg.ItemsSource = dt.DefaultView;
      }

      if (type == "aud")
      {
        string sql = "select * from web_auds";
        DataTable dt = mysql.SqlSelect(sql);
        dg.ItemsSource = dt.DefaultView;
      }

      _type = type;
    }

    void OK()
    { 
        if (dg.SelectedItem == null) return;
      DataRowView drv = (DataRowView)dg.SelectedItem;
 //     if (_type == "tutor")
      {
        ID = drv.Row["id"].ToString();
        isToAll = (bool)cb_ToAll.IsChecked;
        DialogResult = true;
      }
 }
    private void b_ok_Click(object sender, RoutedEventArgs e)
    {
      OK();
     }

    private void b_cancel_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = false;
    }

    private void dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {

    }

    private void dg_KeyDown(object sender, KeyEventArgs e)
    {
      if(e.Key == Key.Enter)   OK();
    }

    }
  public class CReplace
  {
    public string s;
    public string d;
  }
}
