﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MySql;
using System.Data;
using Classes;
namespace Webinars
{
  /// <summary>
  /// Логика взаимодействия для wndZayavka.xaml
  /// </summary>
  public partial class wndZayavka : Window
  {
    static bool IsCreated;

    CMySql _mySql;
    //int _year;
    //int _month;
    DateTime _curr_date;
    Period_str _curr_period;
    int _curr_sem;

    CZayavka Zayavka;
    List<string> selected_groups;
    List<Period_str> _period;

    Webinar _wb_edit;
    CWebinars _webinars;
    public bool _reallyCloseWindow = false;
    string MessForCreating; //сообщение об взможности создания заявки
    FreeDays _dt_FreeDays;

    bool _isLoaded;
    bool _isWebinarChanged;

    public wndZayavka(CMySql mySql, int year, int month, int day, CWebinars webinars)
    {
      _isLoaded = false;
      InitializeComponent();

      _mySql = mySql;

      if (Zayavka == null) Zayavka = new CZayavka(_mySql);

      string sql = " select * from cdo.period_spr";
      DataTable dt = _mySql.SqlSelect(sql);
      _period = new List<Period_str>();
      foreach (DataRow row in dt.Rows) _period.Add(new Period_str(row));

      cb_Period.ItemsSource = _period;
      cb_Period.DisplayMemberPath = "Period";


      Set(null, year, month, day, webinars);

      if (!IsCreated) IsCreated = !IsCreated;
      else
      {
        MessageBox.Show("Окно заявки уже создано");
      }
      selected_groups = new List<string>();

    }
    void GetPeriod()
    {
      string byear = _curr_date.Year.ToString();
      int sem = 1;
      if (_curr_date.Month < 9)
      {
        byear = (_curr_date.Year - 1).ToString();
        DateTime dt_2sem = new DateTime(_curr_date.Year, 2, 22);
        if (_curr_date >= dt_2sem)
        {
          sem = 2;
        }
      }

      foreach (Period_str ps in _period)
        if (ps.BYear == byear)
        {
          cb_Period.SelectedItem = ps;
          if ((_curr_period != ps) || (_curr_sem != sem))
          {
            GetTutors(ps, sem);
          }
          _curr_period = ps;
        }
      _curr_sem = sem;

      cb_numSem.SelectedIndex = _curr_sem - 1;

    }

    public void Set(Webinar webinar, int year, int month, int day, CWebinars webinars)
    {
      _webinars = webinars;
      _wb_edit = webinar;
      _isWebinarChanged = false;

      _curr_date = (_wb_edit == null) ? new DateTime(year, month, day) : webinar.DT;
      dp_date.SelectedDate = _curr_date;
      UpdateData();
      _isLoaded = true;

      if (_wb_edit == null)
      {
        b_clear_dg_free.Visibility = System.Windows.Visibility.Collapsed;
      }
      else
      {
        b_clear_dg_free.Visibility = System.Windows.Visibility.Visible;

      }

    }
    void UpdateData()
    {
      GetPeriod();
      if (_wb_edit != null)
      {
        for (int i = 0; i < cb_Tutor.Items.Count; i++)
        {
          CTutor tutor = (CTutor)cb_Tutor.Items[i];
          if (tutor.TUTOR_ID == _wb_edit.ID_TUTOR)
          {
            cb_Tutor.SelectedItem = cb_Tutor.Items[i];
            cb_Disc.ItemsSource = null;
            cb_Disc.ItemsSource = tutor.Disc;
            for (int j = 0; j < cb_Disc.Items.Count; j++)
            {
              CDisc disc = (CDisc)cb_Disc.Items[j];
              if (disc.ID == _wb_edit.ID_DISC)
              {
                cb_Disc.SelectedItem = cb_Disc.Items[j];
                cb_Disc.SelectedIndex = j;
                if (!IsLoaded) UpdateGroups();
                break;
              }
            }
            break;
          }
        }
        tb_Subject.Text = _wb_edit.SUBJECT;
        tb_url_enter.Text = _wb_edit.URL_ENTER;
        tb_url_record.Text = _wb_edit.URL_RECORD;
        tb_Comment.Text = _wb_edit.Comment;

        Title = "Редактирование заявки";
        tb_web.Text = "Изменяемый вебинар";
        _curr_date = _wb_edit.DT;
        dp_date.SelectedDate = _curr_date;
      }
      else
      {
        if (cb_Disc.Items.Count > 0) cb_Disc.SelectedIndex = 0;
        if (cb_Tutor.Items.Count > 0) cb_Tutor.SelectedIndex = 0;
        Title = "Новая заявка";
        tb_web.Text = "Добавляемый вебинар";
        cb_Tutor.IsEnabled = true;
        dp_date.IsEnabled = true;
        cb_Disc.IsEnabled = true;

        tb_Subject.Text = "";
        tb_url_enter.Text = "";
        tb_url_record.Text = "";

        UpdateDiscForTutor();
        UpdateGroups();

      }
      UpdateInfo();
      tb_err.Text = "";
    }

    void UpdateDiscForTutor()
    {
      if (!_isLoaded) return;

      if (cb_Tutor.SelectedItem == null) return;
      CTutor tutor = (CTutor)cb_Tutor.SelectedItem;
      tutor.Disc = tutor.Disc;
      cb_Disc.ItemsSource = null;
      cb_Disc.ItemsSource = tutor.Disc;
      if (cb_Disc.Items.Count > 0) cb_Disc.SelectedItem = cb_Disc.Items[0];
    }
    void GetTutors(Period_str ps, int numSem)
    {
      if (ps == null) return;
      List<CTutor> lt = Zayavka.GetTutors(ps.BYear, ps.EYear, numSem);

      cb_Tutor.ItemsSource = lt;
      cb_Tutor.DisplayMemberPath = "FIO";

      cb_Tutor.SelectedIndex = 0;

      ClearFinding();
    }

    private void cb_Tutor_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!_isLoaded) return;
      if (cb_Disc.Items.Count > 0) cb_Disc.SelectedIndex = 0;
      ClearFinding();
      UpdateDiscForTutor();
    }

    void UpdateGroups()
    {
      try
      {
        ClearFinding();

        CTutor tutor = (CTutor)cb_Tutor.SelectedItem;
        if (tutor == null) return;
        CDisc disc = (CDisc)cb_Disc.SelectedItem;
        if (disc != null)
        {
          if (tutor.GROUPS == null) tutor.GROUPS = new List<string>();
          tutor.GROUPS.Clear();

          //foreach (string s in z.GetGroups(tutor.TUTOR_ID, disc.ID)) tutor.GROUPS.Add(s);
          if ((bool)ch_AllGroups.IsChecked) tutor.GROUPS = Zayavka.GetGroups(tutor.TUTOR_ID, "", cb_numSem.SelectedIndex + 1);
          else tutor.GROUPS = Zayavka.GetGroups(tutor.TUTOR_ID, disc.ID, cb_numSem.SelectedIndex + 1);

          lb_groups.ItemsSource = null;
          lb_groups.ItemsSource = tutor.GROUPS;

          if (_wb_edit != null)
            if (!string.IsNullOrEmpty(_wb_edit.GROUPS))
            {
              string[] wgroups = _wb_edit.GROUPS.Split(new char[] { ',' });
              foreach (string gr in wgroups)
                foreach (object o in lb_groups.Items)
                {
                  if (gr == (string)o)
                    lb_groups.SelectedItems.Add(o);
                }
            }
        }
        Find();
      }
      catch (Exception exc)
      {
        MessageBox.Show("UpdateGroups - " + exc.Message);
      }
    }

    private void cb_Disc_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!IsLoaded) return;
      UpdateGroups();
    }

    private void b_ok_Click(object sender, RoutedEventArgs e)
    {
      if (cb_Disc.SelectedIndex == -1)
      {
        MessageBox.Show("Не выбрана дисциплина");
        return;
      }
      // if (_wb_edit == null)
      {
        if (selected_groups.Count == 0)
        {
          MessageBox.Show("Не выбраны группы");
          return;
        }
        if (dg_free.SelectedItem == null)
        {
          MessageBox.Show("Не выбраны аудитория и время");
          return;
        }
      }
      if (MessForCreating != "")
      {
        MessageBox.Show(MessForCreating);
        return;
      }

      FreeDay drv_free = (FreeDay)dg_free.SelectedItem;
      CTutor tutor = (CTutor)cb_Tutor.SelectedItem;
      string id_disc = tutor.Disc[cb_Disc.SelectedIndex].ID;
      string dis_full_name = tutor.Disc[cb_Disc.SelectedIndex].dis_full_name;
      


      string sql = "";
      try
      {
        if (_wb_edit == null)
        {
          string id_web_auds = "";
          string id_web_times = "";
          string dt = "";

          dt = _curr_date.Day + "." + _curr_date.Month + "." + _curr_date.Year;
          id_web_auds = drv_free.id_web_auds;
          id_web_times = drv_free.id_web_times;

          sql = "insert into web_total (dt, id_disc, id_tutor, id_web_auds, id_web_times, subject, url_enter, url_record, comment1) values (to_date('" + dt + "', 'dd.MM.yyyy'), " + id_disc +
            ", " + tutor.TUTOR_ID + ",  " + id_web_auds + ",  " + id_web_times + ",  '" + tb_Subject.Text + "',  '" + tb_url_enter.Text + "',  '" + tb_url_record.Text + "',  '" + tb_Comment.Text + "')";
          _mySql.SqlExec(sql);

          sql = "select max(id) from web_total";
          DataTable t = _mySql.SqlSelect(sql);
          string id = t.Rows[0][0].ToString();

          foreach (string s in selected_groups)
          {
            sql = "insert into web_groups (group_id, id_web_total) values('" + s + "', " + id + ")";
            _mySql.SqlExec(sql);
          }
          _webinars.AddNewWebinar(id, _curr_date);
          OnNNewWebinarEvent((Webinar)_webinars.webinars[_webinars.webinars.Count - 1]);

          if (MessageBox.Show("Заявка создана. \r\nПродолжить?", "Завершение", MessageBoxButton.YesNo) == MessageBoxResult.No) Close();
          else
          {
            ClearFinding();
          }
        }
        else //edit
        {
          if (drv_free != null)
          {
            _wb_edit.ID_WEB_AUDS = drv_free.id_web_auds;
            _wb_edit.AUD = drv_free.Aud;
            _wb_edit.ID_WEB_TIMES = drv_free.id_web_times;
            _wb_edit.TIME = drv_free.Time;
            _wb_edit.DT = _curr_date;
            _wb_edit.ID_DISC = id_disc;
            _wb_edit.DIS_FULL_NAME = dis_full_name;
            _wb_edit.FIO = tutor.FIO;
          }
          string dt = _curr_date.Day + "." + _curr_date.Month + "." + _curr_date.Year;
          sql = "update web_total set dt =  to_date('" + dt + "', 'dd.MM.yyyy'), " + " id_disc = " + id_disc + ", id_tutor = " + tutor.TUTOR_ID + ", id_web_auds = " + _wb_edit.ID_WEB_AUDS +
            ", id_web_times = " + _wb_edit.ID_WEB_TIMES + ", subject = '" + tb_Subject.Text + "', url_enter = '" + tb_url_enter.Text + "', url_record = '" + tb_url_record.Text + "', comment1 = '" + tb_Comment.Text + "' where id = " + _wb_edit.ID;
          _mySql.SqlExec(sql);
          _wb_edit.SUBJECT = tb_Subject.Text;
          _wb_edit.URL_ENTER = tb_url_enter.Text;
          _wb_edit.URL_RECORD = tb_url_record.Text;
          _wb_edit.Comment = tb_Comment.Text;

          if (drv_free != null)
          {
            sql = "delete from web_groups where id_web_total = " + _wb_edit.ID;
            _mySql.SqlExec(sql);

            foreach (string s in selected_groups)
            {
              sql = "insert into web_groups (group_id, id_web_total) values('" + s + "', " + _wb_edit.ID + ")";
              _mySql.SqlExec(sql);
              sql = "select CDO.web_get_groups_string(WTL.ID) as GROUPS from CDO.WEB_TOTAL wtl where WTL.ID=" + _wb_edit.ID;
              DataTable t = _mySql.SqlSelect(sql);
              _wb_edit.GROUPS = t.Rows[0][0].ToString();
            }
          }
          Close();
          OnNNewWebinarEvent(_wb_edit);

        }
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
    }

    void Find()
    {
      MessForCreating = "";
      if (lb_groups.SelectedItems.Count == 0) return;
      CTutor tutor = (CTutor)cb_Tutor.SelectedItem;
      string groups = "";
      selected_groups.Clear();
      for (int i = 0; i < lb_groups.SelectedItems.Count; i++)
      {
        string s = (string)lb_groups.SelectedItems[i];
        groups += "'" + s + "',";
        selected_groups.Add(s);

      }
      int ind = groups.LastIndexOf(',');
      groups = groups.Substring(0, ind);

      _dt_FreeDays = Zayavka.FindFree(groups, _curr_date.Year.ToString(), _curr_date.Month.ToString(), _curr_date.Day.ToString());

      dg_free.ItemsSource = _dt_FreeDays.freeRes;
      dg_busy.ItemsSource = null;

      string idTimesBusy = "-1";
      foreach (FreeDay fd in _dt_FreeDays.freeRes)
      {
        if (fd.IsBusy == 1)
        {
          idTimesBusy = idTimesBusy + "," + fd.id_web_times;
        }
      }

      if (_wb_edit != null)
      {
        for (int i = 0; i < dg_free.Items.Count; i++)
        {
          FreeDay fd = (FreeDay)dg_free.Items[i];

          if (fd.id_web_times == _wb_edit.ID_WEB_TIMES)
          {
            dg_free.SelectedIndex = i;
            break;
          }
        }
      }

      UpdateBusy(idTimesBusy);
      UpdateInfo();
    }
    private void lb_groups_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      Find();
      //     UpdateGroups();
    }

    void UpdateInfo()
    {
      if (lb_groups.SelectedItems.Count == 0)
      {
        tb_info.Text = "";
        return;
      }
      string groups = "";
      for (int i = 0; i < lb_groups.SelectedItems.Count; i++)
      {
        string s = (string)lb_groups.SelectedItems[i];
        groups += "'" + s + "',";
      }
      int ind = groups.LastIndexOf(',');
      groups = groups.Substring(0, ind);

      tb_info.Text = groups;
      if (dg_free.SelectedItem != null)
      {
        FreeDay drv = (FreeDay)dg_free.SelectedItem;
        tb_info.Text = tb_info.Text + " на  " + _curr_date.ToShortDateString() + "  " + drv.Time + " в " + drv.Aud;
      }
      tb_err.Text = MessForCreating;

      if (this.ActualHeight < gr_main.ActualHeight) this.Height = gr_main.ActualHeight + 20;
    }
    void UpdateBusy(string idTimesBusy)
    {

      UpdateInfo();

      string sql = "select wg.group_id, WTS.TIME, WAS.AUD, T.LASTNAME || ' ' || T.FIRSNAME || ' ' || T.MIDLENAME as FIO, wtl.id" +
       " from web_groups wg" +
       " join CDO.web_total wtl on wtl.ID = wg.ID_web_total" +
       " join CDO.TUTOR t on T.ID = WTL.ID_TUTOR" +
       " join CDO.WEB_TIMES wts on WTL.ID_WEB_TIMES = WTS.ID" +
       " join CDO.WEB_AUDS was on WAS.ID = WTL.ID_WEB_AUDS" +
       " where id_web_total in (select id from web_total where dt= " + Zayavka.to_date(_curr_date.Year.ToString(), _curr_date.Month.ToString(), _curr_date.Day.ToString()) +
        //      " and id_web_auds = " + fday.id_web_auds + " and id_web_times = " + fday.id_web_times + ")";
  " and id_web_times in (" + idTimesBusy + "))";
      DataTable dt = _mySql.SqlSelect(sql);

      int count = dt.Rows.Count;
      for (int i = 0; i < count; i++)
      {
        DataRow dr = dt.Rows[i];
        bool IsIn = false;
        foreach (string group in selected_groups)
          if (group == dr["group_id"].ToString())
          {
            IsIn = true;
            break;
          }
        if (!IsIn)
        {
          dt.Rows.RemoveAt(i);
          count--;
          i--;
        }
      }

      dg_busy.ItemsSource = dt.DefaultView;
    }
    private void dg_free_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (dg_free.SelectedItem == null) return;
      FreeDay drv_free = (FreeDay)dg_free.SelectedItem;
      CTutor tutor = (CTutor)cb_Tutor.SelectedItem;

      DataTable dt_web_total = null;

      string sql = "select * from web_total where dt = " + Zayavka.to_date(_curr_date.Year.ToString(), _curr_date.Month.ToString(), _curr_date.Day.ToString()) + " and id_web_times = " + drv_free.id_web_times + " and id_tutor=" + tutor.TUTOR_ID;
      dt_web_total = _mySql.SqlSelect(sql);
      //if (_wb_edit != null)
      //{
      //  if (_wb_edit.ID_WEB_TIMES != drv_free.id_web_times)
      //  {
      //    dt_web_total = _mySql.SqlSelect(sql);
      //  }
      //}
      //else
      //{
      //  dt_web_total = _mySql.SqlSelect(sql);
      //}


      MessForCreating = "";
      bool IsGroup  = false;
      if(_wb_edit !=null) 
        IsGroup = (drv_free.id_web_auds == _wb_edit.ID_WEB_AUDS) && (drv_free.id_web_times == _wb_edit.ID_WEB_TIMES);
      
      if (drv_free.IsBusy == 1)
      {
        string m = "Имеется группа на одно время в разных аудиториях \r\n";
        if (_wb_edit == null) MessForCreating += m;
        else
        {
          if (IsGroup) m = "";
          MessForCreating += m;
        }

      }

      if ((dt_web_total != null) && (dt_web_total.Rows.Count > 0))
      {
        string m = "У преподавателя уже есть вебинар на выбранные дату и время \r\n";
        if (_wb_edit == null) MessForCreating += m;
        else
        {
          if ((IsGroup) && (_curr_date == _wb_edit.DT)) m = "";
          MessForCreating += m;
        }
      }
      if (drv_free.CountFreePlaces <= 0)
      {
        MessForCreating += "Нет свободных посадочных мест";
      }

      UpdateInfo();

    }

    //string MessForCreating()
    //{
    //  string MessForCreating = "";

    //  FreeDay drv_free = (FreeDay)dg_free.SelectedItem;
    //  CTutor tutor = (CTutor)cb_Tutor.SelectedItem;

    //  if (drv_free.IsBusy == 1)
    //  {
    //    MessForCreating += "Имеется группа на одно время в разных аудиториях \r\n";
    //  }

    //  if ((dt != null) && (dt.Rows.Count > 0))
    //  {
    //    MessForCreating += "У преподавателя уже есть вебинар на выбранные дату и время \r\n";
    //  }
    //  if (drv_free.CountFreePlaces <= 0)
    //  {
    //    MessForCreating += "Нет свободных посадочных мест";
    //  }

    //  return MessForCreating;
    //}

    void ClearFinding()
    {
      dg_free.ItemsSource = null;
      dg_busy.ItemsSource = null;
      tb_info.Text = "";
      //tb_Subject.Text = "";
      //tb_url_enter.Text = "";
      //tb_url_record.Text = "";

      CTutor tutor = (CTutor)cb_Tutor.SelectedItem;
      if (tutor == null) return;
      lb_groups.ItemsSource = null;
      lb_groups.ItemsSource = tutor.GROUPS;
    }
    private void Button_Click(object sender, RoutedEventArgs e)
    {
      DataRowView drv = (DataRowView)dg_busy.SelectedItem;
      if (MessageBox.Show("Убрать группу " + drv.Row["group_id"] + " у " + drv.Row["fio"], "Удаление", MessageBoxButton.YesNo) == MessageBoxResult.No) return;

      string sql = "select count(*) from web_groups where id_web_total=" + drv.Row["id"];
      DataTable dt = _mySql.SqlSelect(sql);
      if (Convert.ToInt32(dt.Rows[0][0]) <= 1)
      {
        MessageBox.Show("Последняя группа \r\n Удаление запрещено");
        return;
      }

      sql = "delete from web_groups where group_id = '" + drv.Row["group_id"] + "' and id_web_total=" + drv.Row["id"];
      _mySql.SqlExec(sql);

      Find();
    }
    private void cb_Period_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!_isLoaded) return;
      UpdateData();
    }
    private void b_cancel_Click(object sender, RoutedEventArgs e)
    {
      if (_isWebinarChanged)
      {
        if (MessageBox.Show("Вебинар был изменён.\r\n Продолжить?", "Необратимые изменения", MessageBoxButton.YesNo) == MessageBoxResult.Yes) Close();
      }
      else { Close(); }
    }
    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      if (!this._reallyCloseWindow)
      {
        e.Cancel = true;
        Hide();
      }
    }
    private void ch_AllGroups_Checked(object sender, RoutedEventArgs e)
    {
      UpdateGroups();
    }
    private void ch_AllGroups_Unchecked(object sender, RoutedEventArgs e)
    {
      UpdateGroups();
    }
    private void dp_date_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!_isLoaded) return;
      _curr_date = (DateTime)dp_date.SelectedDate;
      GetPeriod();
      //     Set(null, _curr_date.Year, _curr_date.Month, _curr_date.Day, _webinars);

      Find();
    }
    private void cb_numSem_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!_isLoaded) return;
      UpdateDiscForTutor();
    }


    public delegate void NewWebinarEventHandler(Object sender, Webinar args);
    public event NewWebinarEventHandler NewWebinarHandler;
    protected virtual void OnNNewWebinarEvent(Webinar e)
    {
      if (NewWebinarHandler != null)
      {
        NewWebinarHandler(this, e);
      }
    }

    private void b_clear_dg_free_Click(object sender, RoutedEventArgs e)
    {
      string sql = "delete from Web_groups where id_web_total = " + _wb_edit.ID;
      _mySql.SqlExec(sql);
      Find();
      _isWebinarChanged = true;
    }
  }
  public class KeyBusyDayColor : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      try
      {
        int st = (int)value;
        if (st == 1) return Brushes.LightGreen;
        if (st == 0) return Brushes.Red;
      }
      catch
      {
      }
      return Brushes.Silver;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      Visibility visibility = (Visibility)value;
      return (visibility == Visibility.Visible);
    }
  }


}
