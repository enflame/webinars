﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MySql;
using System.Data;
using System.IO;
using Elements;
using Classes;
using System.ComponentModel;
using System.Globalization;
namespace Webinars
{
  /// <summary>
  /// Логика взаимодействия для MainWindow.xaml
  /// </summary> 
  public partial class MainWindow : Window
  {
    CMySql _mySql;

    CWebinars _webinars;

    int _year;
    int _month;
    int _day;
    List<SqlFilter> SqlFilters;
    List<SqlSort> SqlSorts;
    wndZayavka _wndZayavka;

    List<CReplace> _replaceElements;
    public MainWindow()
    {
      InitializeComponent();
      try
      {
        _mySql = new CMySql();
        _mySql.ConnectToDB();
        _year = DateTime.Now.Year;
        _month = DateTime.Now.Month;
        _day = DateTime.Now.Day;
        cb_year.Items.Clear();
        cb_year.Items.Add(2014);
        cb_year.Items.Add(2015);
        cb_year.Items.Add(2016);
        cb_year.SelectedItem = _year;


        SqlFilters = new List<SqlFilter>();
        SqlSorts = new List<SqlSort>();
        GlobalParams.Path = AppDomain.CurrentDomain.BaseDirectory;
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }


      cb_month.SelectedIndex = _month - 1;
      cb_month.SelectedItem = _year.ToString();

      System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
      dispatcherTimer.Tick += new EventHandler(AfterloadAllElements);
      dispatcherTimer.Interval = new TimeSpan(0, 0, 5);
      dispatcherTimer.Start();
      tb_Status.Text = "Загрузка данных";
      //   UpdateWebinars();
    }
    private void AfterloadAllElements(object sender, EventArgs e)
    {
      System.Windows.Threading.DispatcherTimer dispatcherTimer = (System.Windows.Threading.DispatcherTimer)sender;
      dispatcherTimer.Stop();

      NewWebinars();
      tb_Status.Text = "";
    }

    bool IsInList(List<string> list, string value)
    {
      bool res = false;
      foreach (string s in list)
        if (s == value) return true;
      return res;
    }
    List<string> GetListValues(DataTable dt, string field)
    {
      List<string> res = new List<string>();
      foreach (DataRow dr in dt.Rows)
        if (!IsInList(res, dr[field].ToString())) res.Add(dr[field].ToString());

      return res;
    }
    void SetFilters()
    {
      foreach (SqlFilter f in SqlFilters)
      {
        if (f.Name == "f_groups") f.Set(_webinars.FGroups);
        if (f.Name == "f_fio") f.Set(_webinars.FFio);
        if (f.Name == "f_time") f.Set(_webinars.FTime);
        if (f.Name == "f_aud") f.Set(_webinars.FAud);
        if (f.Name == "f_dayOfweek") f.Set(_webinars.FdayOfweek);


        if (f.Name == "f_data") f.Set(_webinars.FDT);
        if (f.Name == "f_disc") f.Set(_webinars.FDisc);
        if (f.Name == "f_subject") f.Set(_webinars.FSubject);
        if (f.Name == "f_url_enter") f.Set(_webinars.FUrl_enter);
        if (f.Name == "f_url_record") f.Set(_webinars.FUrl_record);
      }
    }

    void UpdateSorting()
    {
      dg_tutors.Items.SortDescriptions.Clear();
      foreach (SqlSort s in SqlSorts)
      {
        if (s.type_sorted != SqlSort.not_sorted) dg_tutors.Items.SortDescriptions.Add(s.SD);

      }
    }
    void NewWebinars()
    {
      try
      {
        DateTime dt1 = new DateTime(_year, _month, 1);
        DateTime dt2 = new DateTime(_year, _month, DateTime.DaysInMonth(dt1.Year, dt1.Month));
        _webinars = new CWebinars(dt1, dt2, _mySql);

        int y = Convert.ToInt32(cb_year.Text);
        int m = cb_month.SelectedIndex + 1;
        int d = DateTime.Now.Day;
        DateTime dt = DateTime.Now; ;
        if ((dt.Year != y) | (dt.Month != m)) d = 1;


        dg_tutors.Items.SortDescriptions.Clear();

        dg_tutors.ItemsSource = _webinars.webinars;

        SetFilters();
        UpdateSorting();

        if (_wndZayavka != null)
        {
          //  _wndZayavka.Set(null, y, m, d, _webinars);
        }
        else
        {
          _wndZayavka = new wndZayavka(_mySql, _year, _month, _day, _webinars);
          _wndZayavka.NewWebinarHandler += new wndZayavka.NewWebinarEventHandler(wnd_NewWebinarHandler);
        }
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
    }

    void wnd_NewWebinarHandler(object sender, Webinar args)
    {
      dg_tutors.Items.Refresh();
      if ((args.DT.Year != _year) || (args.DT.Month != _month))
      {
        _year = args.DT.Year;
        _month = args.DT.Month;
        cb_year.SelectedItem = _year;
        cb_month.SelectedIndex = _month;

        NewWebinars();
      }
      //throw new NotImplementedException();
    }

    private void f_groups_Loaded(object sender, RoutedEventArgs e)
    {
      SqlFilter f = (SqlFilter)sender;

      SqlFilters.Add(f);

      f.SqlFilterHandler += new SqlFilter.SqlFilterEventHandler(f_SqlFilterFilterHandler);

    }
    private void s_aud_Loaded(object sender, RoutedEventArgs e)
    {
      SqlSort s = (SqlSort)sender;
      if (s.Name == "s_groups") s.Set("GROUPS");
      if (s.Name == "s_fio") s.Set("FIO");
      if (s.Name == "s_time") s.Set("TIME");
      if (s.Name == "s_aud") s.Set("AUD");
      if (s.Name == "s_data") s.Set("DT");

      if (s.Name == "s_disc") s.Set("DIS_FULL_NAME");
      if (s.Name == "s_subject") s.Set("SUBJECT");
      if (s.Name == "s_url_enter") s.Set("URL_ENTER");
      if (s.Name == "s_url_record") s.Set("URL_RECORD");
      SqlSorts.Add(s);
      s.SqlSortHandler += new SqlSort.SqlSortEventHandler(s_SqlSortHandler);
    }

    void s_SqlSortHandler(object sender, SqlSort.SqlSortEventArgs args)
    {
      UpdateSorting();
      //     foreach (SortDescription sd in dg_tutors.Items.SortDescriptions)
      //     {
      //       if (sd.PropertyName == args.obj.PropertyName)
      //       {
      //         dg_tutors.Items.SortDescriptions.Remove(sd);
      //         break;
      //       }
      //     }
      //     if (args.type_sorted != SqlSort.not_sorted)
      //     {
      //       dg_tutors.Items.SortDescriptions.Add(args.obj);
      ////       dg_tutors.Items.Refresh();
      //     }
      //     throw new NotImplementedException();
    }
    void f_SqlFilterFilterHandler(object sender, SqlFilter.SqlFilterEventArgs args)
    {
      try
      {
        SqlFilter f = (SqlFilter)sender;
        if (f.Name == "f_dayOfweek") f.SetImage(_webinars.SetFilter("DayOfWeek"));
        if (f.Name == "f_groups") f.SetImage(_webinars.SetFilter("GROUPS"));
        if (f.Name == "f_fio") f.SetImage(_webinars.SetFilter("FIO"));
        if (f.Name == "f_time") f.SetImage(_webinars.SetFilter("TIME"));
        if (f.Name == "f_aud") f.SetImage(_webinars.SetFilter("AUD"));
        if (f.Name == "f_data") f.SetImage(_webinars.SetFilter("DT"));

        if (f.Name == "f_disc") f.SetImage(_webinars.SetFilter("DIS_FULL_NAME"));
        if (f.Name == "f_subject") f.SetImage(_webinars.SetFilter("SUBJECT"));
        if (f.Name == "f_url_enter") f.SetImage(_webinars.SetFilter("URL_ENTER"));
        if (f.Name == "f_url_record") f.SetImage(_webinars.SetFilter("URL_RECORD"));
        if (f.Name == "f_url_record") f.SetImage(_webinars.SetFilter("URL_RECORD"));
        dg_tutors.Items.Refresh();

        //  throw new NotImplementedException();
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }

    }

    private void b_NewZayavka_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        int y = Convert.ToInt32(cb_year.Text);
        int m = cb_month.SelectedIndex + 1;
        int d = DateTime.Now.Day;
        DateTime dt = DateTime.Now; ;
        if ((dt.Year != y) | (dt.Month != m)) d = 1;

         _wndZayavka.Show();
         _wndZayavka.Activate();
         _wndZayavka.Set(null, y, m, d, _webinars);
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
      //SetFilters();
      //UpdateSorting();
    }
    private void b_edit_Click(object sender, RoutedEventArgs e)
    {
      if (dg_tutors.SelectedItem == null) return;
      Webinar wb = (Webinar)dg_tutors.SelectedItem;
      _wndZayavka.Set(wb, wb.DT.Year, wb.DT.Month, wb.DT.Day, _webinars);

      try
      {
        _wndZayavka.Show();
        _wndZayavka.Activate();
        dg_tutors.Items.Refresh();

        SetFilters();
        UpdateSorting();
      }
      catch (Exception exc)
      {
      }
    }
    private void dg_tutors_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      b_edit_Click(null, null);
    }

    private void cb_year_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!IsLoaded) return;
      if (e.AddedItems.Count == 0) return;
      _year = (int)cb_year.SelectedItem;
      NewWebinars();
    }
    private void cb_month_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!IsLoaded) return;
      if (e.AddedItems.Count == 0) return;
      _month = cb_month.SelectedIndex + 1;
      NewWebinars();
    }

    private void b_del_Click(object sender, RoutedEventArgs e)
    {
      if (dg_tutors.SelectedItem == null) return;
      Webinar wb = (Webinar)dg_tutors.SelectedItem;
      if (MessageBox.Show(wb.FIO + "\r\n" + wb.DIS_FULL_NAME + "\r\n" + wb.AUD + " " + wb.TIME + "?", "Удаление", MessageBoxButton.YesNo) == MessageBoxResult.No) return;
      string sql = "delete from WEB_TOTAL where id = " + wb.ID;
      _mySql.SqlExec(sql);

      sql = "delete from WEB_GROUPS where id_web_total = " + wb.ID;
      _mySql.SqlExec(sql);
      _webinars.webinars.Remove(wb);
      dg_tutors.Items.Refresh();

      SetFilters();
      UpdateSorting();
    }
    private void b_export_Click(object sender, RoutedEventArgs e)
    {
      Microsoft.Office.Interop.Excel.Application ExcelApp = null;
      Microsoft.Office.Interop.Excel.Worksheet ExcelWorkSheet;
      Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = null;
      try
      {
        ExcelApp = new Microsoft.Office.Interop.Excel.Application();
        ExcelApp.DisplayAlerts = true;
        ExcelWorkBook = ExcelApp.Workbooks.Add(GlobalParams.Path + "Расписание вебинаров.xltm");
        ExcelApp.Visible = true;


        ExcelWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)ExcelWorkBook.Worksheets.get_Item(1);
        Microsoft.Office.Interop.Excel.Range xlR = ExcelWorkSheet.UsedRange;
        int numRow = 3;
        foreach (Webinar web in _webinars.webinars)
        {
          xlR.Cells[numRow, 2] = web.AUD;
          xlR.Cells[numRow, 3] = web.DayOfWeek;
          xlR.Cells[numRow, 4] = web.DT;
          xlR.Cells[numRow, 5] = web.TIME;
          xlR.Cells[numRow, 6] = web.FIO;
          xlR.Cells[numRow, 7] = web.DIS_FULL_NAME;
          xlR.Cells[numRow, 8] = web.SUBJECT;
          xlR.Cells[numRow, 9] = web.GROUPS;
          xlR.Cells[numRow, 10] = web.URL_ENTER;
          xlR.Cells[numRow, 11] = web.URL_RECORD;

          System.Windows.Media.Color mcolor = Colors.Green;

          if (web.ID_WEB_AUDS == "0") mcolor = Colors.LightGreen;
          if (web.ID_WEB_AUDS == "1") mcolor = Colors.LightBlue;
          if (web.ID_WEB_AUDS == "2") mcolor = (Color)ColorConverter.ConvertFromString("#FFFFFF96");
          if (web.ID_WEB_AUDS == "3") mcolor = Colors.Orange;

          System.Drawing.Color color = System.Drawing.Color.FromArgb(mcolor.A, mcolor.R, mcolor.G, mcolor.B);

          //Microsoft.Office.Interop.Excel.Range xlR1 = ExcelWorkSheet.get_Range(xlR.Cells[numRow, 2], xlR.Cells[numRow, 11]);
          //  xlR1.Interior.Color = System.Drawing.ColorTranslator.ToOle(color);

          (xlR.Cells[numRow, 2] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
          (xlR.Cells[numRow, 3] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
          (xlR.Cells[numRow, 4] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
          (xlR.Cells[numRow, 5] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
          (xlR.Cells[numRow, 6] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
          (xlR.Cells[numRow, 7] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
          (xlR.Cells[numRow, 8] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
          (xlR.Cells[numRow, 9] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
          (xlR.Cells[numRow, 10] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
          (xlR.Cells[numRow, 11] as Microsoft.Office.Interop.Excel.Range).Interior.Color = System.Drawing.ColorTranslator.ToOle(color);

          numRow++;
        }
        xlR.EntireRow.AutoFit();

        ExcelWorkSheet.Name = (string)((ComboBoxItem)cb_month.SelectedItem).Content;
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
        ExcelApp.Quit();
        return;
      }

    }
    string ToString(object o)
    {
      if (o == null) return "";
      return o.ToString();
    }
    private void b_import_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
        string path = "";
        if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        {
          path = dlg.FileName;
        }
        else return;

        int list1;
        int list2;

        wndImportData wnd1 = new wndImportData();
        if (!(bool)wnd1.ShowDialog()) return;
        else
        {
          list1 = wnd1.list1;
          list2 = wnd1.list2;
        }


        Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
        ExcelApp.DisplayAlerts = false;

        Microsoft.Office.Interop.Excel.Worksheet ExcelWorkSheet;
        Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = null;
        try
        {
          ExcelWorkBook = ExcelApp.Workbooks.Open(path);
        }
        catch (Exception exc)
        {
          MessageBox.Show(exc.Message);
          ExcelApp.Quit();
          return;
        }
        if (_replaceElements == null) _replaceElements = new List<CReplace>();

        for (int numList = list1; numList <= list2; numList++)
        {
          ExcelWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)ExcelWorkBook.Worksheets.get_Item(numList);
          Microsoft.Office.Interop.Excel.Range xlRange = ExcelWorkSheet.UsedRange;

          char[] delT = new char[1] { ' ' };
          char[] delG = new char[2] { ',', '\n' };
          for (int nrow = 3; nrow < 2000; nrow++)
          {
            try
            {
              string aud = xlRange.Cells[nrow, 2].Value2.ToString();
              string day = xlRange.Cells[nrow, 3].Value2.ToString();
              string dt = (DateTime.FromOADate(Convert.ToDouble(xlRange.Cells[nrow, 4].Value2.ToString()))).ToShortDateString();
              string time = DateTime.FromOADate(Convert.ToDouble(xlRange.Cells[nrow, 5].Value2.ToString())).ToShortTimeString();

              string fio = ToString(xlRange.Cells[nrow, 6].Value2);
              string disc = ToString(xlRange.Cells[nrow, 7].Value2);
              string subj = ToString(xlRange.Cells[nrow, 8].Value2);
              string groups = ToString(xlRange.Cells[nrow, 9].Value2);
              string enter = ToString(xlRange.Cells[nrow, 10].Value2);
              string record = ToString(xlRange.Cells[nrow, 11].Value2);
              string comment = ToString(xlRange.Cells[nrow, 12].Value2);

              aud = aud.Replace("\"", " ").Trim();


              string id_tutor = " ";
              string id_disc = " ";
              string id_web_auds = " ";
              string id_web_times = " ";

              string sql = "";
              DataTable table = null;
              //tutor
              if (fio == "") id_tutor = "null";
              else
              {
                string[] fioT = fio.Split(delT);
                sql = "select * from tutor T" +
                " where t.lastname = '" + fioT[0].Trim() + "' and firsname = '" + fioT[1].Trim() + "' and midlename = '" + fioT[2].Trim() + "'";
                table = _mySql.SqlSelect(sql);
                if (table.Rows.Count == 0)
                {
                  string res = NotFoundElement("tutor", fio, _mySql, nrow);
                  if (string.IsNullOrEmpty(res)) break;
                  id_tutor = res;
                }
                else
                {
                  id_tutor = table.Rows[0]["id"].ToString();
                }
              }
              //group
              List<string> selected_groups = new List<string>();
              if (groups != "")
              {
                string[] groupsT = groups.Split(delG);
                for (int i = 0; i < groupsT.Length; i++)
                {
                  string gr = groupsT[i].Trim();
                  if (gr != "")
                  {
                    sql = "select * from stud_group where id='" + gr + "'";
                    table = _mySql.SqlSelect(sql);
                    if (table.Rows.Count == 0)
                    {
                      string res = NotFoundElement("group", gr, _mySql, nrow + 1);
                      if (string.IsNullOrEmpty(res)) break;
                      selected_groups.Add(res);
                    }
                    else selected_groups.Add(gr);
                  }
                }
              }

              //disc
              if (disc == "") id_disc = "null";
              else
              {
                sql = " select distinct sd.dis_full_name as disc_name, sd.id" +
                              " from S_DISCIPLINE sd" +
                              " where SD.ID in (select distinct TL.DISCIPLINE_ID from TUTORING_LOAD tl  where TL.TUTOR_ID = " + id_tutor + ")" +
                              " and sd.dis_full_name = '" + disc + "'";
                table = _mySql.SqlSelect(sql);
                if (table.Rows.Count == 0)
                {
                  string res = NotFoundElement("disc", disc, _mySql, nrow + 1, id_tutor);
                  if (string.IsNullOrEmpty(res)) break;
                  id_disc = res;
                }
                else
                {
                  id_disc = table.Rows[0]["id"].ToString();
                }
              }
              //auds
              sql = " select * from web_auds where aud = '" + aud + "'";
              table = _mySql.SqlSelect(sql);
              if (table.Rows.Count == 0)
              {

                string res = NotFoundElement("aud", aud, _mySql, nrow + 1);
                if (string.IsNullOrEmpty(res)) break; 
                id_web_auds = res;
              }
              else
              {
                id_web_auds = table.Rows[0]["id"].ToString();
              }
              //          
              sql = " select * from web_times where time = '" + time + "'";
              table = _mySql.SqlSelect(sql);
              if (table.Rows.Count == 0)
              {
                string res = NotFoundElement("time", time, _mySql, nrow + 1);
                if (string.IsNullOrEmpty(res)) break;
                id_web_times = res;

              }
              else
              {
                id_web_times = table.Rows[0]["id"].ToString();
              }
              //
              if (!(string.IsNullOrEmpty(fio) || string.IsNullOrEmpty(disc)))
              {
                sql = "insert into web_total (dt, id_disc, id_tutor, id_web_auds, id_web_times, subject, url_enter, url_record, comment1) values (to_date('" + dt + "', 'dd.MM.yyyy'), " + id_disc +
                   ", " + id_tutor + ",  " + id_web_auds + ",  " + id_web_times + ",  '" + subj + "',  '" + enter + "',  '" + record + "',  '" + comment + "')";
                _mySql.SqlExec(sql);

                sql = "select max(id) from web_total";
                DataTable t = _mySql.SqlSelect(sql);
                string id = t.Rows[0][0].ToString();

                foreach (string s in selected_groups)
                {
                  sql = "insert into web_groups (group_id, id_web_total) values('" + s + "', " + id + ")";
                  _mySql.SqlExec(sql);
                }
              }
            }
            catch (Exception exc)
            {
              MessageBox.Show("Строка №" + nrow + " данных нет (" + exc.Message + "\r\nИмпорт текщего листа закончен");
              break;
            }
          }
        }
        ExcelApp.Quit();
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
      NewWebinars();
    }

    string NotFoundElement(string type, string element, CMySql mysql, int numRow, string addit = "")
    {
      foreach (CReplace r in _replaceElements)
      {
        if (element == r.s) return r.d;
      }
      wndImportNotFound wnd = new wndImportNotFound(type, element, mysql, numRow, addit);
      if ((bool)wnd.ShowDialog())
      {
        if (wnd.isToAll)
        {
          _replaceElements.Add(new CReplace() { s = element, d = wnd.ID });
        }
        return wnd.ID;
      }
      return null;
    }

    private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
    {
      double h = ActualHeight - sp_bottom.ActualHeight - sp_top.ActualHeight - 50;
      if (h > 0) dg_tutors.Height = h;
      dg_tutors.Width = ActualWidth - 20;
    }
    private void dg_tb_date_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
      ((DataView)dg_tutors.ItemsSource).Sort = "DT";
    }
    private void b_clear_Click(object sender, RoutedEventArgs e)
    {
      if (MessageBox.Show("Удалить всё за выбранный период?", "Удаление", MessageBoxButton.YesNo) == MessageBoxResult.No) return;
      for (int i = 0; i < dg_tutors.Items.Count; i++)
      {

        Webinar drv = (Webinar)dg_tutors.Items[i];
        string sql = "delete from WEB_TOTAL where id = " + drv.ID;
        _mySql.SqlExec(sql);

        sql = "delete from WEB_GROUPS where id_web_total = " + drv.ID;
        _mySql.SqlExec(sql);
      }
      _webinars.webinars.Clear();
      dg_tutors.Items.Refresh();
      ClearAllFiltrSort();
    }

    private void Window_Closed(object sender, EventArgs e)
    {
      if (_wndZayavka != null)
      {
        _wndZayavka._reallyCloseWindow = true;
        _wndZayavka.Close();
      }
    }

    void ClearAllFiltrSort()
    {
      _webinars.ClearAllFiltr();
      foreach (SqlFilter sf in SqlFilters)
      {
        sf.SetImage(false);
      }
      dg_tutors.Items.Refresh();

      SetFilters();
      foreach (SqlSort ss in SqlSorts) ss.Sort(SqlSort.not_sorted);
      UpdateSorting();
    }
    private void b_ClearAllFiltrSort_Click(object sender, RoutedEventArgs e)
    {
      ClearAllFiltrSort();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
    }

    private void b_RenewData_Click(object sender, RoutedEventArgs e)
    {
      NewWebinars();
      ClearAllFiltrSort();
    }






  }
}
