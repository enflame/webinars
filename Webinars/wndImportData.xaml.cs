﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Webinars
{
  /// <summary>
  /// Логика взаимодействия для wndImportData.xaml
  /// </summary>
  public partial class wndImportData : Window
  {
    public int list1;
    public int list2;
    public wndImportData()
    {
      InitializeComponent();
    }

    private void b_ok_Click(object sender, RoutedEventArgs e)
    {
      list1 = cb_list1.SelectedIndex + 1;
      list2 = cb_list2.SelectedIndex + 1;
      DialogResult = true;

    }

    private void b_cancel_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = false;

    }
  }
}
