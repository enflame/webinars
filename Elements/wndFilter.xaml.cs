﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Elements
{
  /// <summary>
  /// Логика взаимодействия для wndFilter.xaml
  /// </summary>
  /// 
  public partial class wndFilter : Window
  {
    List<object> FilterData;
    public wndFilter()
    {
      InitializeComponent();
    }
    public void Set(List<object> FilterData)
    {
      lb_sort.Items.Clear();
      foreach (object o in FilterData)
      {
        CheckBox chb = new CheckBox();
        chb.Checked += new RoutedEventHandler(chb_Checked);
        chb.Unchecked += new RoutedEventHandler(chb_Unchecked);
        Binding myBinding = new Binding("IsSelected");
        chb.SetBinding(CheckBox.IsCheckedProperty, myBinding);

        myBinding = new Binding("Value");
        chb.SetBinding(CheckBox.ContentProperty, myBinding);

        myBinding = new Binding("IsVisible");
        chb.SetBinding(CheckBox.VisibilityProperty, myBinding);

        chb.DataContext = o;
        lb_sort.Items.Add(chb);
      }
      this.FilterData = FilterData;
    }

    void chb_Unchecked(object sender, RoutedEventArgs e)
    {
      foreach (object o in FilterData)
      {
        Classes.FilterData fd = (Classes.FilterData)o;
        if (fd.IsSelected == true)
        {
  //        Chb_SelectAll.IsChecked = null;
          return;
        }
      }
  //    Chb_SelectAll.IsChecked = false;
    }
    void chb_Checked(object sender, RoutedEventArgs e)
    {

      foreach (object o in FilterData)
      {
        Classes.FilterData fd = (Classes.FilterData)o;
        if (fd.IsSelected == false)
        {
  //        Chb_SelectAll.IsChecked = null;
          return;
        }
      }
  //    Chb_SelectAll.IsChecked = true;

      //throw new NotImplementedException();
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
    }

    private void Window_Deactivated(object sender, EventArgs e)
    {
      Hide();
    }

    private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
    {

      lb_sort.Height = ActualHeight - 30 - tb_filtr.ActualHeight - b_apply.ActualHeight;
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {

    }

    void SetTxtFiltr()
    {
      string txt = tb_filtr.Text;
      bool isFinded = false;
      foreach (object o in FilterData)
      {
        Classes.FilterData fd = (Classes.FilterData)o;
        if ((bool)chb_inAnyPlace.IsChecked)
        {
          if (fd.Value.ToLower().IndexOf(txt.ToLower()) != -1)
          {
            fd.IsSelected = true;
            isFinded = true;
          }
          else fd.IsSelected = false;
        }
        else
        {
          if (fd.Value.ToLower().IndexOf(txt.ToLower()) == 0)
          {
            fd.IsSelected = true;
            isFinded = true;
          }
          else fd.IsSelected = false;
        }
      }
      UpdateDataContext();

      if (isFinded) Chb_SelectAll.IsChecked = null;
    }
    private void tb_filtr_TextChanged(object sender, TextChangedEventArgs e)
    {
      SetTxtFiltr();
    }
    void UpdateDataContext()
    {
      foreach (object o in lb_sort.Items)
      {
        CheckBox chb = (CheckBox)o;
        object oo = chb.DataContext;
        chb.DataContext = null;
        chb.DataContext = oo;
      }
    }
    private void Window_Activated(object sender, EventArgs e)
    {
      b_removeFiltr.Visibility = System.Windows.Visibility.Hidden;
      foreach (object o in FilterData)
      {
        Classes.FilterData fd = (Classes.FilterData)o;
        if (!fd.IsSelected)
        {
          b_removeFiltr.Visibility = System.Windows.Visibility.Visible;
          break;
        }
      }

      UpdateDataContext();
      tb_filtr.Focus();
    }

    private void Chb_SelectAll_Checked(object sender, RoutedEventArgs e)
    {
      if (!IsLoaded) return;

      tb_filtr.Text = "";
      foreach (object o in FilterData)
      {
        Classes.FilterData fd = (Classes.FilterData)o;
        fd.IsSelected = true;
      }
      UpdateDataContext();
    }
    private void Chb_SelectAll_Unchecked(object sender, RoutedEventArgs e)
    {
      if (!IsLoaded) return;

      tb_filtr.Text = "";
      foreach (object o in FilterData)
      {
        Classes.FilterData fd = (Classes.FilterData)o;
        fd.IsSelected = false;
      }
      UpdateDataContext();
    }

    private void b_apply_Click(object sender, RoutedEventArgs e)
    {
      Hide();
    }

    private void chb_inAnyPlace_Unchecked(object sender, RoutedEventArgs e)
    {
      SetTxtFiltr();
    }
    private void chb_inAnyPlace_Checked(object sender, RoutedEventArgs e)
    {
      SetTxtFiltr();
    }

    private void Window_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter) Hide();
    }

    private void lb_sort_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
    {
      foreach (object o in lb_sort.Items)
      {
      }
    }

    private void b_removeFiltr_Click(object sender, RoutedEventArgs e)
    {
      tb_filtr.Text = "";
      foreach (object o in FilterData)
      {
        Classes.FilterData fd = (Classes.FilterData)o;
        fd.IsSelected = true;
        fd.IsVisible = System.Windows.Visibility.Visible;
      }
      UpdateDataContext();
      b_removeFiltr.Visibility = System.Windows.Visibility.Hidden;
    }


  }
}
