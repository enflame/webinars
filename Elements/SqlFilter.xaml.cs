﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Classes;
namespace Elements
{
  /// <summary>
  /// Логика взаимодействия для SqlFilter.xaml
  /// </summary>
  public partial class SqlFilter : UserControl
  {
  
    wndFilter wnd;
    bool IswndActive;
     public SqlFilter()
    {
      InitializeComponent();
      b_Filtered.Visibility = System.Windows.Visibility.Collapsed;
      IswndActive = false;
    }

    public void SetImage(bool isFiltered)
    {
      if (isFiltered)
      {
        b_unFiltered.Visibility = System.Windows.Visibility.Collapsed;
        b_Filtered.Visibility = System.Windows.Visibility.Visible;
      }
      else
      {
        b_Filtered.Visibility = System.Windows.Visibility.Collapsed;
        b_unFiltered.Visibility = System.Windows.Visibility.Visible;
      }
    }
    public void Set(List<object> source)
    {
      if (wnd == null) wnd = new wndFilter();
      wnd.Set(source);

      //wnd.lb_sort.DisplayMemberPath = "Value";
      //wnd.lb_sort.SelectedMemberPath = "IsSelected";
      //wnd.lb_sort.ValueMemberPath = "IsSelected";
    }
    
    private void b_sort_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        IswndActive = true;
        Image im = (Image)sender;
        Point p = PointToScreen(Mouse.GetPosition(im));
        wnd.Left = p.X;
        wnd.Top = p.Y;
        wnd.Show();
        wnd.Deactivated += new EventHandler(wnd_Closed);
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
      }
    }
    private void b_sort_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      b_sort_Click(sender, null);
    }

    void wnd_Closed(object sender, EventArgs e)
    {
      if (IswndActive)
      {
        SqlFilterEventArgs args = new SqlFilterEventArgs(); args.TypeEvent = SqlFilterEventArgs.IsSetFilter;
        OnSqlFilterEvent(args);
      }
     IswndActive = false;
      //throw new NotImplementedException();
    }


    #region Events
    public class SqlFilterEventArgs : EventArgs
    {
      public static int IsSetFilter = 1;

      public int TypeEvent;
      public object obj;
    }
    public delegate void SqlFilterEventHandler(Object sender, SqlFilterEventArgs args);
    public event SqlFilterEventHandler SqlFilterHandler;
    protected virtual void OnSqlFilterEvent(SqlFilterEventArgs args)
    {
      if (SqlFilterHandler != null)
      {
        SqlFilterHandler(this, args);
      }
    }
    #endregion Events

    private void UserControl_Unloaded(object sender, RoutedEventArgs e)
    {
      if (wnd != null) wnd.Close();
    }

 
 
  }
}
