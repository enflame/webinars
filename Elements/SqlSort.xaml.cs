﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ComponentModel;
namespace Elements
{
  /// <summary>
  /// Логика взаимодействия для SqlSort.xaml
  /// </summary>
  public partial class SqlSort : UserControl
  {
    public static int not_sorted = -1;
    public static int up_sorted = 1;
    public static int down_sorted = 0;

    public int type_sorted { get; set; }

    SortDescription _SD;
    public SortDescription SD { get { return _SD; } }
    wndSort wnd;
    public SqlSort()
    {
      InitializeComponent();
      im_DownSort.Visibility = System.Windows.Visibility.Collapsed;
      im_UpSort.Visibility = System.Windows.Visibility.Collapsed;
      im_noSort.Visibility = System.Windows.Visibility.Visible;
      type_sorted = SqlSort.not_sorted;
    }

    public void Set(string SortMemberPath)
    {
      _SD = new SortDescription(SortMemberPath, ListSortDirection.Ascending);
      _SD.PropertyName = SortMemberPath;
    }

    private void im_noSort_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      if (wnd == null) wnd = new wndSort();

      Image im = (Image)sender;
      Point p = PointToScreen(Mouse.GetPosition(im));
      wnd.Left = p.X;
      wnd.Top = p.Y;
      wnd.Show();
      wnd.Deactivated += new EventHandler(wnd_Closed);
    }
    public SqlSortEventArgs Sort(int type_sorted)
    {
      this.type_sorted = type_sorted;
      SqlSortEventArgs args = new SqlSortEventArgs();

      if (type_sorted == SqlSort.not_sorted)
      {
        args.type_sorted = SqlSort.not_sorted;
        im_DownSort.Visibility = System.Windows.Visibility.Collapsed;
        im_UpSort.Visibility = System.Windows.Visibility.Collapsed;
        im_noSort.Visibility = System.Windows.Visibility.Visible;
      }
      if (type_sorted == SqlSort.up_sorted)
      {
        args.type_sorted = SqlSort.up_sorted;
        _SD.Direction = ListSortDirection.Descending;

        im_DownSort.Visibility = System.Windows.Visibility.Collapsed;
        im_UpSort.Visibility = System.Windows.Visibility.Visible;
        im_noSort.Visibility = System.Windows.Visibility.Collapsed;
      }
      if (type_sorted == SqlSort.down_sorted)
      {
        args.type_sorted = SqlSort.down_sorted;
        _SD.Direction = ListSortDirection.Ascending;

        im_DownSort.Visibility = System.Windows.Visibility.Visible;
        im_UpSort.Visibility = System.Windows.Visibility.Collapsed;
        im_noSort.Visibility = System.Windows.Visibility.Collapsed;
      }
      args.obj = _SD;
      return args;
    }
    void wnd_Closed(object sender, EventArgs e)
    {
      SqlSortEventArgs args = Sort(wnd.type_sorted);
       args.obj = _SD;
      OnSqlFilterEvent(args);
      //throw new NotImplementedException();
    }

    #region Events
    public class SqlSortEventArgs : EventArgs
    {
      public int type_sorted;
      public SortDescription obj;
    }
    public delegate void SqlSortEventHandler(Object sender, SqlSortEventArgs args);
    public event SqlSortEventHandler SqlSortHandler;
    protected virtual void OnSqlFilterEvent(SqlSortEventArgs args)
    {
      if (SqlSortHandler != null)
      {
        SqlSortHandler(this, args);
      }
    }
    #endregion Events 

    private void UserControl_Unloaded(object sender, RoutedEventArgs e)
    {
      if (wnd != null) wnd.Close();
    }

  }
}
