﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Elements
{
  /// <summary>
  /// Логика взаимодействия для wndSort.xaml
  /// </summary>
  public partial class wndSort : Window
  {
 
    public int type_sorted;
    public wndSort()
    {
      InitializeComponent();
    }



    private void b_not_sorted_Click(object sender, RoutedEventArgs e)
    {
      type_sorted = SqlSort.not_sorted;
      Hide();
    }
    private void b_up_sorted_Click(object sender, RoutedEventArgs e)
    {
      type_sorted = SqlSort.up_sorted;
      Hide();

    }
    private void b_down_sorted_Click(object sender, RoutedEventArgs e)
    {
      type_sorted = SqlSort.down_sorted;
      Hide();

    }

    private void Window_Deactivated(object sender, EventArgs e)
    {
      Hide();
    }
  }
}
 