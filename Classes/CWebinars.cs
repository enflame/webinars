﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using MySql;
namespace Classes
{

  public class CWebinars
  {
    public List<object> webinars;

    public List<object> FGroups;
    public List<object> FFio;
    public List<object> FTime;
    public List<object> FAud;
    public List<object> FdayOfweek;
    public List<object> FDT;

    public List<object> FDisc;
    public List<object> FSubject;
    public List<object> FUrl_enter;
    public List<object> FUrl_record;

    List<List<object>> Filters;
    List<string> FilterNames;

    public DateTime _dt1;
    public DateTime _dt2;
    CMySql _mySql;

    DataTable _dt_times;

    public CWebinars(DateTime dt1, DateTime dt2, CMySql mySql)
    {
      _dt1 = dt1;
      _dt2 = dt2;
      _mySql = mySql;
      string sql = "select * from web_times";
      _dt_times = _mySql.SqlSelect(sql);

      //       DataTable dt_web = SelectWebinars("");
      List<DataTable> dt_web = SelectWebinars();

      webinars = new List<object>();



      FGroups = new List<object>(); FFio = new List<object>(); FTime = new List<object>(); FAud = new List<object>();
      FDT = new List<object>(); FDisc = new List<object>(); FSubject = new List<object>(); FUrl_enter = new List<object>();
      FUrl_record = new List<object>(); FdayOfweek = new List<object>();

      Filters = new List<List<object>>();
      Filters.Add(FGroups); Filters.Add(FFio); Filters.Add(FTime); Filters.Add(FAud); Filters.Add(FDT); Filters.Add(FDisc);
      Filters.Add(FSubject); Filters.Add(FUrl_enter); Filters.Add(FUrl_record); Filters.Add(FdayOfweek);

      FilterNames = new List<string>();
      FilterNames.Add("GROUPS"); FilterNames.Add("FIO"); FilterNames.Add("TIME"); FilterNames.Add("AUD"); FilterNames.Add("DT"); FilterNames.Add("DIS_FULL_NAME");
      FilterNames.Add("SUBJECT"); FilterNames.Add("URL_ENTER"); FilterNames.Add("URL_RECORD"); FilterNames.Add("DayOfWeek");


      foreach (DataTable dt in dt_web)
      {
        AddWebinars(dt);
      }
    }


    List<DataTable> SelectWebinars()
    {
      DateTime dt = new DateTime(_dt1.Year, _dt1.Month, _dt1.Day);
      TimeSpan ts = new TimeSpan(1, 0, 0, 0);
      List<DataTable> res = new List<DataTable>();
      while (dt <= _dt2)
      {
        foreach (DataRow dr_time in _dt_times.Rows)
        {
          string id_web_times = dr_time["id"].ToString();
          string sql = " select WTL.ID, WTL.ID_WEB_AUDS , WA.AUD, WTL.DT,  WTL.ID_WEB_TIMES, WT.TIME, WTL.ID_TUTOR, T.LASTNAME || ' ' || T.FIRSNAME || ' ' || T.MIDLENAME as FIO, WTL.ID_DISC,  DS.DIS_FULL_NAME, WTL.SUBJECT," +
                " CDO.web_get_groups_string(WTL.ID) as GROUPS, WTL.URL_ENTER, WTL.URL_RECORD, wtl.comment1" +
                " from CDO.WEB_TOTAL wtl" +
                " join CDO.WEB_AUDS wa on WA.ID = WTL.ID_WEB_AUDS" +
                " join CDO.WEB_TIMES wt on WT.ID = WTL.ID_WEB_TIMES" +
                " join CDO.TUTOR t on T.ID = WTL.ID_TUTOR" +
                " join CDO.S_DISCIPLINE ds on DS.ID = WTL.ID_DISC" +
                " where wtl.dt=to_date('" + dt.ToShortDateString() + "', 'dd.MM.yyyy') and wtl.id_web_times=" + id_web_times;
          sql += " order by WA.ID, WTL.DT, TIME, FIO";
          DataTable dt_web = _mySql.SqlSelect(sql);
            res.Add(dt_web);
        }
        dt += ts;
      }
      //DataTable dt_res = new DataTable();
      //dt_res.
      return res;
    }
    DataTable SelectWebinars(string web_total_id)
    {
      string sql = " select WTL.ID, WTL.ID_WEB_AUDS , WA.AUD, WTL.DT,  WTL.ID_WEB_TIMES, WT.TIME, WTL.ID_TUTOR, T.LASTNAME || ' ' || T.FIRSNAME || ' ' || T.MIDLENAME as FIO, WTL.ID_DISC,  DS.DIS_FULL_NAME, WTL.SUBJECT," +
            " CDO.web_get_groups_string(WTL.ID) as GROUPS, WTL.URL_ENTER, WTL.URL_RECORD, wtl.comment1" +
            " from CDO.WEB_TOTAL wtl" +
            " join CDO.WEB_AUDS wa on WA.ID = WTL.ID_WEB_AUDS" +
            " join CDO.WEB_TIMES wt on WT.ID = WTL.ID_WEB_TIMES" +
            " join CDO.TUTOR t on T.ID = WTL.ID_TUTOR" +
            " join CDO.S_DISCIPLINE ds on DS.ID = WTL.ID_DISC" +
            " where wtl.dt>=to_date('" + _dt1.ToShortDateString() + "', 'dd.MM.yyyy') and wtl.dt<=to_date('" + _dt2.ToShortDateString() + "', 'dd.MM.yyyy')";
      if (web_total_id != "") sql += "and WTL.ID = " + web_total_id;
      sql += " order by WA.ID, WTL.DT, TIME";

      DataTable dt_web = _mySql.SqlSelect(sql);

       return dt_web;
    }
    void AddWebinars(DataTable dt_web)
    {
      foreach (DataRow dr in dt_web.Rows)
      {
        Webinar wb = new Webinar();
        wb.AUD = dr["AUD"].ToString();
        //  wb.AUD_ID = dr["AUD_ID"].ToString();
        wb.DIS_FULL_NAME = dr["DIS_FULL_NAME"].ToString();
        wb.DT = (DateTime)dr["DT"];
        wb.FIO = dr["FIO"].ToString();
        wb.ID = dr["ID"].ToString();
        wb.ID_DISC = dr["ID_DISC"].ToString();
        wb.ID_TUTOR = dr["ID_TUTOR"].ToString();
        wb.ID_WEB_AUDS = dr["ID_WEB_AUDS"].ToString();
        wb.ID_WEB_TIMES = dr["ID_WEB_TIMES"].ToString();
        wb.SUBJECT = dr["SUBJECT"].ToString();
        wb.TIME = dr["TIME"].ToString();
        wb.GROUPS = dr["GROUPS"].ToString();
        wb.URL_ENTER = dr["URL_ENTER"].ToString();
        wb.URL_RECORD = dr["URL_RECORD"].ToString();
        wb.Comment = dr["Comment1"].ToString();
        wb.IsRowVis = true;


        webinars.Add(wb);

        AddToFilter(FGroups, wb.GROUPS);
        AddToFilter(FFio, wb.FIO);
        AddToFilter(FTime, wb.TIME);
        AddToFilter(FAud, wb.AUD);
        AddToFilter(FDT, wb.DT.ToShortDateString());
        AddToFilter(FdayOfweek, wb.DayOfWeek);

        AddToFilter(FDisc, wb.DIS_FULL_NAME);
        AddToFilter(FSubject, wb.SUBJECT);
        AddToFilter(FUrl_enter, wb.URL_ENTER);
        AddToFilter(FUrl_record, wb.URL_RECORD);
        //
      }
      FGroups.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });
      FFio.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });
      FTime.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });
      FAud.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });
      FDT.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });
      FdayOfweek.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });
      FDisc.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });
      FSubject.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });
      FUrl_enter.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });
      FUrl_record.Sort(delegate(object p1, object p2) { int compareDate = ((FilterData)p1).Value.CompareTo(((FilterData)p2).Value); return compareDate; });

    }

    //public Webinar GetWebinar(string web_total_id, DateTime dt)
    //{
    //  DataTable dt_web = SelectWebinars(web_total_id);

    //}
    public void AddNewWebinar(string web_total_id, DateTime dt)
    {
      if (!((dt >= _dt1) & (dt <= _dt2))) return;
      DataTable dt_web = SelectWebinars(web_total_id);
      AddWebinars(dt_web);
    }
    void AddToFilter(List<object> filter, string value)
    {
      foreach (FilterData fd in filter)
        if (fd.Value == value) return;
      filter.Add(new FilterData() { Value = value, IsSelected = true });
    }
    void HideFilter(List<object> filter, string field)
    {
      foreach (FilterData fd in filter)
      {
        fd.IsVisible = System.Windows.Visibility.Collapsed;
        foreach (Webinar wb in webinars)
        {
          string value = "";
          switch (field)
          {
            case "DayOfWeek":
              value = wb.DayOfWeek;
              break;
            case "AUD":
              value = wb.AUD;
              break;
            case "FIO":
              value = wb.FIO;
              break;
            case "TIME":
              value = wb.TIME;
              break;
            case "GROUPS":
              value = wb.GROUPS;
              break;

            case "DT":
              value = wb.DT.ToShortDateString();
              break;
            case "DIS_FULL_NAME":
              value = wb.DIS_FULL_NAME;
              break;
            case "SUBJECT":
              value = wb.SUBJECT;
              break;
            case "URL_ENTER":
              value = wb.URL_ENTER;
              break;
            case "URL_RECORD":
              value = wb.URL_RECORD;
              break;
          }
          if ((wb.IsRowVis) & (fd.Value == value))
          {
            fd.IsVisible = System.Windows.Visibility.Visible;
            break;
          }
        }
      }
    }


    public void ClearFiltr(List<object> filter, string field)
    {
      foreach (FilterData fd in filter)
      {
        fd.IsSelected = true;
        fd.IsVisible = System.Windows.Visibility.Visible;
      }
      //  SetFilter(field);
    }

    public void ClearAllFiltr()
    {
      foreach (Webinar wb in webinars)
      {
        wb.IsRowVis = true;
      }

      for (int i = 0; i < FilterNames.Count; i++)
      {
        ClearFiltr(Filters[i], FilterNames[i]);
      }
    }

    void UpdateFilters(string field)
    {
      if (field != "DayOfWeek") HideFilter(FdayOfweek, "DayOfWeek");
      if (field != "GROUPS") HideFilter(FGroups, "GROUPS");
      if (field != "FIO") HideFilter(FFio, "FIO");
      if (field != "TIME") HideFilter(FTime, "TIME");
      if (field != "AUD") HideFilter(FAud, "AUD");
      if (field != "DT") HideFilter(FDT, "DT");

      if (field != "DIS_FULL_NAME") HideFilter(FDisc, "DIS_FULL_NAME");
      if (field != "SUBJECT") HideFilter(FSubject, "SUBJECT");
      if (field != "URL_ENTER") HideFilter(FUrl_enter, "URL_ENTER");
      if (field != "URL_RECORD") HideFilter(FUrl_record, "URL_RECORD");
    }
    public bool SetFilter(string field)
    {
      List<object> filter = null;
      switch (field)
      {
        case "DayOfWeek":
          filter = FdayOfweek;
          break;
        case "AUD":
          filter = FAud;
          break;
        case "FIO":
          filter = FFio;
          break;
        case "TIME":
          filter = FTime;
          break;
        case "GROUPS":
          filter = FGroups;
          break;

        case "DT":
          filter = FDT;
          break;
        case "DIS_FULL_NAME":
          filter = FDisc;
          break;
        case "SUBJECT":
          filter = FSubject;
          break;
        case "URL_ENTER":
          filter = FUrl_enter;
          break;
        case "URL_RECORD":
          filter = FUrl_record;
          break;
      }

      bool isSetted = false;
      foreach (FilterData fd in filter)
      {
        //   if (fd.IsSelected)
        foreach (Webinar wb in webinars)
        {
          string value = fd.Value;
          switch (field)
          {
            case "DayOfWeek":
              if (wb.DayOfWeek == value) wb.IsDayOfWeekSelect = fd.IsSelected;
              break;
            case "AUD":
              if (wb.AUD == value) wb.IsAUDSelect = fd.IsSelected;
              break;
            case "FIO":
              if (wb.FIO == value) wb.IsFIOSelect = fd.IsSelected;
              break;
            case "TIME":
              if (wb.TIME == value) wb.IsTIMESelect = fd.IsSelected;
              break;
            case "GROUPS":
              if (wb.GROUPS == value) wb.IsGROUPSSelect = fd.IsSelected;
              break;

            case "DT":
              if (wb.DT.ToShortDateString() == value) wb.IsDTSelect = fd.IsSelected;
              break;
            case "DIS_FULL_NAME":
              if (wb.DIS_FULL_NAME == value) wb.IsDIS_FULL_NAMESelect = fd.IsSelected;
              break;
            case "SUBJECT":
              if (wb.SUBJECT == value) wb.IsSUBJECTSelect = fd.IsSelected;
              break;
            case "URL_ENTER":
              if (wb.URL_ENTER == value) wb.IsURL_ENTERSelect = fd.IsSelected;
              break;
            case "URL_RECORD":
              if (wb.URL_RECORD == value) wb.IsURL_RECORDSelect = fd.IsSelected;
              break;
          }
        }
        if (!fd.IsSelected) isSetted = true;
      }
      //      if ((!isSetted) & (filter.Count != filter_all.Count)) isSetted = true; ;
      UpdateFilters(field);

      return isSetted;
    }
  }

  public class FilterData : IEquatable<FilterData>, IComparable<FilterData>
  {
    public string Value { get; set; }
    public bool IsSelected { get; set; }
    public System.Windows.Visibility IsVisible { get; set; }

    public FilterData()
    {
      IsSelected = false;
      IsVisible = System.Windows.Visibility.Visible;
    }

    public override bool Equals(object obj)
    {
      if (obj == null) return false;
      FilterData objAsPart = obj as FilterData;
      if (objAsPart == null) return false;
      else return Equals(objAsPart);
    }
    // Default comparer for Part type.
    public int CompareTo(FilterData comparePart)
    {
      // A null value means that this object is greater.
      if (comparePart == null)
        return 1;

      else
        return this.Value.CompareTo(comparePart.Value);
    }
    public bool Equals(FilterData other)
    {
      if (other == null) return false;
      return (this.Value.Equals(other.Value));
    }
  }
}
