﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Classes
{
  public static class GlobalParams
  {
    public static string Path { get; set; }
    public static string FileSettings { get { return Path + "settings.dat"; } }
    public static string PathShablons { get { return Path + "Шаблоны\\"; } }
  }
}
