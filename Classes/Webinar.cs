﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Globalization;
namespace Classes
{
  public class Webinar
  {
    public string ID { get; set; }
    public string ID_WEB_AUDS { get; set; }
    public string AUD { get; set; }
    public string DayOfWeek
    {
      get
      {
        CultureInfo c = new CultureInfo("ru-RU");
        return c.DateTimeFormat.GetDayName(DT.DayOfWeek);
        //return DT.DayOfWeek.ToString(new CultureInfo("ru-RU"));
      }
    }//рассчитывается
    //  public string AUD_ID { get; set; }
    public DateTime DT { get; set; }
    public string ID_WEB_TIMES { get; set; }
    public string TIME { get; set; }
    public string ID_TUTOR { get; set; }
    public string FIO { get; set; }
    public string ID_DISC { get; set; }
    public string DIS_FULL_NAME { get; set; }
    public string SUBJECT { get; set; }
    public string GROUPS { get; set; }
    public string URL_ENTER { get; set; }
    public string URL_RECORD { get; set; }
    public string Comment { get; set; }


    public bool IsFIOSelect { get; set; }
    public bool IsTIMESelect { get; set; }
    public bool IsDTSelect { get; set; }
    public bool IsAUDSelect { get; set; }
    public bool IsGROUPSSelect { get; set; }

    public bool IsDIS_FULL_NAMESelect { get; set; }
    public bool IsSUBJECTSelect { get; set; }
    public bool IsURL_ENTERSelect { get; set; }
    public bool IsURL_RECORDSelect { get; set; }
    public bool IsDayOfWeekSelect { get; set; }

    public bool IsRowVis
    {
      get
      {
        return (IsFIOSelect & IsTIMESelect & IsDTSelect & IsAUDSelect & IsGROUPSSelect & IsDIS_FULL_NAMESelect &
                IsSUBJECTSelect & IsURL_ENTERSelect & IsURL_RECORDSelect & IsDayOfWeekSelect);
      }
      set
      {
        IsFIOSelect = value;
        IsTIMESelect = value;
        IsDTSelect = value;
        IsAUDSelect = value;

        IsGROUPSSelect = value;
        IsDIS_FULL_NAMESelect = value;
        IsSUBJECTSelect = value;
        IsURL_ENTERSelect = value;
        IsURL_RECORDSelect = value;
        IsDayOfWeekSelect = value;


      }
    }

  }
}
